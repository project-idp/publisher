from flask import Flask, request

import pika


app = Flask(__name__)



@app.route("/publish", methods=['POST'])
def publish():
    event = request.form["event"]
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue='task_queue', durable=True)
    channel.basic_publish(
        exchange='',
        routing_key='task_queue',
        body=event,
        properties=pika.BasicProperties(
            delivery_mode=2,
        ))
    connection.close()
    return "OK"


if __name__ == "__main__":
    #start_http_server(8000)
    app.run(host="0.0.0.0")
